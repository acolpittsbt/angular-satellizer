var app = angular.module('DemoApp', [
	'ui.router',
	'satellizer'
]);

app.config(function ($stateProvider, $urlRouterProvider, $authProvider) {
  $stateProvider
	  .state('home', {
			url: '/home',
			templateUrl: 'partials/home.tpl.html'
		})
    .state('dashboard', {
			url: '/dashboard',
			templateUrl: 'partials/dashboard.tpl.html',
			controller: "DashboardController as dashboard",
			data: {requiredLogin: true}
		})
		.state('login', {
			url: '/login',
			templateUrl: 'partials/login.tpl.html',
			controller: 'LoginSignupController as loginSignup'
		});
	
	$urlRouterProvider.otherwise('/home');

	$authProvider.facebook({
		clientId: '746019982207849',
		// by default, the redirect URI is http://localhost:5000
		redirectUri: location.origin + location.pathname
	});

});

app.run(function ($rootScope, $state, $auth) {
	$rootScope.$on('$stateChangeStart',
		function (event, toState) {
			var requiredLogin = false;
			// check if new state requires authentication
			if (toState.data && toState.data.requiredLogin)
				requiredLogin = true;

			// if yes and if this user is not logged in, redirect to login
			if (requiredLogin && !$auth.isAuthenticated()) {
				event.preventDefault();
				$state.go('login');
			}
		});
});


app.controller('LoginSignupController', function($auth, $state) {
	var vm = this;
	vm.signup = signup;
	vm.login = login;
	vm.auth = auth;

	function signup() {
		$auth
			.signup({email: vm.email, password: vm.password})
			.then(function (response) {
				// set the token received from the server
				$auth.setToken(response);
				// go to protected page
				$state.go('dashboard');
			})
			.catch(function (error) {
				console.log("ERROR: ", error);
			})
	}

	function login() {
		$auth
			.login({email: vm.email, password: vm.password})
			.then(function (response) {
				$auth.setToken(response);
				$state.go('dashboard');
			})
			.catch(function (error) {
				console.log('Error: ', error);
			})
	};

	function auth(provider) {
		$auth.authenticate(provider)
			.then(function (response) {
				console.debug("success", response);
				$state.go('dashboard');
			})
			.catch(function (error) {
				console.debug("catch", error);
			})
	}

});

app.controller('DashboardController', function($state, $auth, $http) {
	var vm = this;
	vm.logout = logout;
	vm.user = null;

	function logout() {
		$auth.logout();
		$state.go('home');
	};

	// Initialize
	getUserInfo();

	function getUserInfo() {
		$http.get('/user')
			.then(function (response) {
				vm.user = response.data;
			})
			.catch(function (error) {
				console.log("Error: ", error);
			})
	}

});
